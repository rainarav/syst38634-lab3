import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import java.util.Arrays;
import java.util.Collection;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith (Parameterized.class)
public class TimeTest {

public String timeToTest;
	
	public TimeTest(String dataTime) {
		timeToTest = dataTime;
	}
	
	@Parameterized.Parameters
	public static Collection<Object[]> loadData() {
		Object[][] data = {{"12:05:05"}};
		return Arrays.asList(data);
	}
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testGetTotalSeconds() {
		int seconds = Time.getTotalSeconds(this.timeToTest);
		System.out.println(this.timeToTest);
		assertTrue("The seconds were not calculated properly", seconds == 43505 );
	}
	
	//Test that fails Regular Test
	@Test
	public void testGetMilliSeconds () {
		int millis = Time.getMilliSeconds("12:05:05:05");
		System.out.println(millis);
		assertTrue("The milliseconds were not calculated properly", millis == 5);
	}
	
	// Test for Exception 
	@Test (expected = NumberFormatException.class)
	public void testGetMilliSecondsException () {
		int millis = Time.getMilliSeconds("12:05:05:5555");
		System.out.println(millis);		
	}	
	// Test for Boundary In = Use 999
	@Test
	public void testGetMilliSecondsBoundaryIn () {
		int millis = Time.getMilliSeconds("12:05:05:999");
		System.out.println(millis);
		assertTrue("The milliseconds were not calculated properly", millis == 999);
	}	
	//Test for Boundary Out = Use 1000
	@Test (expected = NumberFormatException.class)
	public void testGetMilliSecondsBoundaryOut () {
		int millis = Time.getMilliSeconds("12:05:05:1000");
		System.out.println(millis);

	}
}
